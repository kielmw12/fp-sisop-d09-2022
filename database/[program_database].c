char activeUser[20] = "";
char activeDB[20] = "";
const char delimiter[10] = " ,=();\n";
const char tempPath[20] = "databases/temp.csv";
const char logPath[20] = "query.log";
int insertData(char namaTabel[], char data[][20], int colAmount)
{
    char path[100];
    sprintf(path, "databases/%s/%s.csv", activeDB, namaTabel);
    FILE *tabelfp = fopen(path, "a+");
    int count = 0;
    while (count < colAmount)
    {
        if (count > 0)
            fprintf(tabelfp, ",");
        fprintf(tabelfp, "%s", data[count++]);
    }
    fprintf(tabelfp, "\n");
    fclose(tabelfp);
    FILE *fpTemp = fopen(tempPath, "w");
    fprintf(fpTemp, "1 row data inserted\n");
    fclose(fpTemp);
    return 1;
}
int updateData(char namaTabel[], char column[], char newValue[],
               char condColumn[], char condValue[])
{
    int res = 0;
    int whereCond = 0;

    char path[100];
    sprintf(path, "databases/%s/%s.csv", activeDB, namaTabel);
    FILE *tabelfp = fopen(path, "r");
    FILE *fpTemp;

    char TableHead[100];
    fgets(TableHead, sizeof(TableHead), tabelfp);

    char columnNames[100][100];
    char tempTableHead[100];

    strcpy(tempTableHead, TableHead);
    int colAmount = splitString(columnNames, tempTableHead);

    if (strcmp(condValue, ""))
        whereCond = 1;

    int count = 0;
    int colIndex = -1;
    int condColIndex = -1;
    while (count < colAmount)
    {
        if (!strcmp(columnNames[count], column))
        {
            res = 1;
            colIndex = count;
        }

        if (!strcmp(columnNames[count], condColumn))
            condColIndex = count;

        if (res && (!whereCond || (whereCond && condColIndex > -1)))
            break;

        count++;
    }
    int rowUpdatedcount = 0;
    if (res)
    {
        char *tempFileName = "temp.csv";
        fpTemp = fopen(tempFileName, "w");
        fputs(TableHead, fpTemp);

        char line[100];
        fgets(line, sizeof(line), tabelfp);
        fputs(line, fpTemp);

        while (fgets(line, sizeof(line), tabelfp) != NULL)
        {
            char columnDatas[100][100];
            char tempLine[100];
            strcpy(tempLine, line);
            colAmount = splitString(columnDatas, tempLine);

            if (whereCond && strcmp(condValue, columnDatas[condColIndex]))
            {
                fputs(line, fpTemp);
                continue;
            }

            int chunkcount = 0;
            while (chunkcount < colAmount)
            {
                if (chunkcount > 0)
                    fprintf(fpTemp, ",");

                if (chunkcount != colIndex)
                {
                    fprintf(fpTemp, "%s", columnDatas[chunkcount]);
                }
                else
                {
                    fprintf(fpTemp, "%s", newValue);
                }

                chunkcount++;
            }
            rowUpdatedcount++;
            fprintf(fpTemp, "\n");
        }

        fclose(tabelfp);
        fclose(fpTemp);

        remove(path);
        rename(tempFileName, path);
    }
    fpTemp = fopen(tempPath, "w");
    fprintf(fpTemp, "%d row data updated\n", rowUpdatedcount);
    fclose(fpTemp);
    return res;
}
